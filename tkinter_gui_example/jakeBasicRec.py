import urllib2
import re
import BeautifulSoup
#from samba.dcerpc.samr import Ids

class ProfScraper(object):
    def __init__(self):
        self.profList = []

    
    #Generates list of professors from given school in
    #the form of tuples (id, firstname, lastname).
    #id is a number given by the website.    
    def genProfList(self, url):
        soup = str(BeautifulSoup.BeautifulSoup(urllib2.urlopen(url)))
        numpages = re.findall('(?<=\"searchResultsTotal\": )\d*', soup)[0]
        numpages = int(numpages) / 20 + 1
        
        for each in range(1, numpages + 1):
            url = re.sub('(?<=page=)\d*', str(each), url)
            soup = str(BeautifulSoup.BeautifulSoup(urllib2.urlopen(url)))
            first = re.findall('(?<="tFname": ")\w*', soup)
            last = re.findall('(?<="tLname": ")\w*', soup)
            department = re.findall('(?<="tDept": ")\w*', soup)
            ratingClass = re.findall('(?<="rating_class": ")\w*', soup)
            rating = re.findall('(?<="overall_rating": ").*(?=")', soup)
            ids = re.findall('(?<="tid": )\d*', soup)
            
            for each in range(0, len(first)):
                self.profList.append((ids[each], first[each], last[each],ratingClass[each], rating[each], department[each]))
        return self.profList
        #print self.profList
        
    #genRates processes the list of professors, and 
    #accesses the ratings for each professor in the list.
    def genRates(self, url):
        i = 1
        for each in self.profList:
            print str(i) + " of 1318 professors processed"
            i = i + 1
            url = re.sub('(?<=tid=)\d*', each[0], url)
            soup = str(BeautifulSoup.BeautifulSoup(urllib2.urlopen(url)))
            cycles = re.findall('(?<="remaining": )\d*', soup)[0]
            cycles = int(cycles)/20 + 2
            filename = "professors/" + str(each[2]) + '_' + str(each[1]) + '_' + str(each[0]) + ".txt"
            f = open(filename, 'w')
            for x in range(1, cycles):
                url = re.sub('(?<=page=)\d*', str(x), url)
                soup = str(BeautifulSoup.BeautifulSoup(urllib2.urlopen(url)))
                ids = re.findall('(?<="id": )\d*', soup)
                classname = re.findall('(?<="rClass": ").*(?=")', soup)
                clarity = re.findall('(?<="rClarity": )\d*', soup)
                easy = re.findall('(?<="rEasy": )\d*', soup)
                helpful = re.findall('(?<="rHelpful": )\d*', soup)
                comments = re.findall('(?<="rComments": ").*(?=")', soup)
                
                for y in range(0, len(ids)):
                    f.write(str((each[0], ids[y], classname[y], clarity[y], easy[y], helpful[y], comments[y])) + '\n')
            f.close()
        print "done"
        
def main(args):
    mainList = []
    scraper = ProfScraper()
    urlProf = "http://www.ratemyprofessors.com/find/professor/?department=&institution=Utah+State+University&page=56&query=*%3A*&queryoption=TEACHER&queryBy=schoolId&sid=4050&sortBy="
    urlRates = "http://www.ratemyprofessors.com/paginate/professors/ratings?tid=54278&page=1"
    mainList = scraper.genProfList(urlProf)

    #so below we see our main list that is generated from scraping and parsing the json
    #objects
    ##was able to manipulate this data a bit and display some stuff that will be used in
        ##my content based recommender
    print mainList
    for index, each in enumerate(mainList):
        if mainList[index][4] == 'N/A':
            next
        elif float(mainList[index][4]) >= 4.0:
            print each
    print "now lets search for computer science"
    for index, each in enumerate(mainList):
        if mainList[index][5] == 'Computer':
            print each
    #not sure why this isn't working currently but we'll roll with it for now
    #scraper.genRates(urlRates)

    ##alright so from here on below we are now going to construct a basic recommender
                #my goal is to make a basic input loop where i will enter professors i have
                #taken and then i will write a function that will recomment professors based
                # off of review scores and department
    myp = ''
    mypRate = 0
    mypDept = ''
    recommendedList=[]
    while(myp != '1'):
        myp = raw_input("Enter a professors Name: ")
        recommendedList=[]
        if myp == '1':
            break
        first, last = myp.split()
        print first
        print last
        for index, each in enumerate(mainList):
            if mainList[index][1] == first and mainList[index][2] == last:
                 mypRate = mainList[index][4]
                 mypDept = mainList[index][5]
        print mypRate
        print mypDept
        
        for index, each in enumerate(mainList):
            if (mainList[index][4] >= mypRate or float(mainList[index][4]) > float(mypRate)-1.0 ) and mainList[index][5] == mypDept and str(mainList[index][4]) != "N/A":
                recommendedList.append((mainList[index][1], mainList[index][2], mainList[index][4]))
        print "Here is your recommended list: "
        print recommendedList
        print "Here is a prettier version of that list: " 
        for i in recommendedList:
            print i
    
    print "I am done" 
    
        
if __name__ == "__main__":
    import sys
    main(sys.argv)
