from sqlalchemy import *
from os import getcwd, listdir
import re

basepath = getcwd()
path = "sqlite:////" + basepath + "/ratings.db"
db = create_engine(path)

metadata = MetaData(db)

ratings = Table('ratings', metadata,
                   Column('prof_id', Integer),
                   Column('name', String),
                   Column('rating_id', Integer, primary_key=True),
                   Column('course', String),
                   Column('helpful', Integer),
                   Column('clarity', Integer),
                   Column('easy', Integer),
                   Column('comments', String)
                   )

ratings.create()

filepath = basepath + "/professors"
print filepath
k = 0
for each in listdir(filepath):
    if "swp" not in each:
        print "Processing ", k, "of 1318 professors"

        k = k + 1
        for line in open(filepath + '/' + each, 'r'):
            profname = re.sub('[a-zA-Z]*_', "", each, 1)
            profname = re.sub('_\d*\.\w*', "", profname)
            profname = profname + ' ' + re.sub('_\w*\..*', "", each)         
            
            line = re.sub('\(', '', line)
            line = re.sub('\)', '', line)
            line = re.sub('\'', '', line)
            line = re.split(',', line, 6)
            i = ratings.insert()
            i.execute(prof_id=line[0], name=profname, rating_id=line[1], course=line[2], helpful=line[3], clarity=line[4], easy=line[5], comments=line[6])    

    
