from sqlalchemy import *
from os import getcwd, listdir
import re

basepath = getcwd()
path = "sqlite:////" + basepath + "/ratings.db"
db = create_engine(path)

metadata = MetaData(db)

professors = Table('professors', metadata, autoload=True)
# 
# professors = Table('professors', metadata,
#                    Column('prof_id', Integer),
#                    Column('name', String),
#                    )

# professors.create()
# 
# filepath = basepath + "/professors"
# print filepath
# k = 0
# for each in listdir(filepath):
#     print "Processing ", k, "of 1318 professors"
#     k = k + 1
#     for line in open(filepath + '/' + each, 'r'):
#         line = re.sub('\(', '', line)
#         line = re.sub('\)', '', line)
#         line = re.sub('\'', '', line)
#         line = re.split(',', line, 6)
#         i = professors.insert()
#         i.execute(prof_id=line[0], name=re.sub('\_\d+.txt', '', each))    

s = professors.select()
rs = s.execute()
for row in rs:
    print "prof_id: ", row['prof_id']
    print "name: ", row['name']
    
