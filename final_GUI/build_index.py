from re import split
import pickle
from sqlalchemy import *
from os import getcwd

# Database Design:
# Column('prof_id', Integer),
# Column('name', String),
# Column('rating_id', Integer, primary_key=True),
# Column('course', String),
# Column('helpful', Integer),
# Column('clarity', Integer),
# Column('easy', Integer),
# Column('comments', String)

basepath = getcwd()
path = "sqlite:////" + basepath + "/ratings.db"
db = create_engine(path)
metadata = MetaData(db)
ratings = Table('ratings', metadata, autoload=True)

class Build_ngram_index(object):
    def __init__(self):
        self.onegrams = {}
        self.twograms = {}
        self.trigrams = {}
        
    def read_db(self):
        s = ratings.select()
        rows = s.execute()
        
        i = 0
        
        for each in rows:
            print i, " ratings processed"
            i += 1
            if each['prof_id'] not in self.onegrams:
                self.onegrams[each['prof_id']] = []
            if each['prof_id'] not in self.twograms:
                self.twograms[each['prof_id']] = []
            if each['prof_id'] not in self.trigrams:
                self.trigrams[each['prof_id']] = []
                
            text = self.tokenize(each['comments'])
            self.index(each['prof_id'], text)
        self.save_dicts()
            
    
    def tokenize(self, text):
        text = text.lower()
        text = split('[^a-zA-Z0-9]', text)
        for each in text:
            if each == "":
                text.remove(each)
        return text
        
    def index(self, prof_id, text):          
        if len(text) > 2:   
            for i in range(0, len(text) - 2):
                self.trigrams[prof_id].append((text[i], text[i + 1], text[i + 2]))
                self.twograms[prof_id].append((text[i], text[i + 1]))
                self.onegrams[prof_id].append(text[i])
            
            self.twograms[prof_id].append((text[-2], text[-1]))
            self.onegrams[prof_id].append(text[-2])
            self.onegrams[prof_id].append(text[-1])
            
        elif len(text) > 1:   
            for i in range(0, len(text) - 1):
                self.twograms[prof_id].append((text[i], text[i + 1]))
                self.onegrams[prof_id].append(text[i])

            self.onegrams[prof_id].append(text[-1])
            
        else:
            self.onegrams[prof_id].append(text[0])  
        
    def save_dicts(self):
        with open('onegrams.pickle', 'wb') as handle:
            pickle.dump(self.onegrams, handle)
        with open('twograms.pickle', 'wb') as handle:
            pickle.dump(self.twograms, handle)
        with open('trigrams.pickle', 'wb') as handle:
            pickle.dump(self.trigrams, handle) 
            

            
def main(args):
    builder = Build_ngram_index()
    builder.read_db()
    
if __name__ == "__main__":
    import sys
    main(sys.argv)        
            