('1959618', '25546216', 'COMM1300', '3', '2', '4', "Jason's class is interesting but his lectures are long and group work is essentially the whole grade. He also has a very strange way of presenting notes and they aren't put online so you have to come to class if you want to pass.")
('1959618', '25161530', 'COMM3600', '5', '5', '5', 'Gilmore is an amazing teacher! One of my favorite teachers I have ever had here at USU. ')
('1959618', '24656716', 'PUBDISCOURSE', '5', '2', '5', 'Gilmore is very epic. Such a class act. Heck of a teacher.')
('1959618', '24023204', 'CMST4300', '4', '3', '5', 'Loved professor Gilmore. He goes above and beyond to help you. There are no tests and no final in this class. But there are lots of papers. You will write a 1 page single spaced paper every week.')
