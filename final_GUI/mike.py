##tryin to segment our code even more... idk

import pickle
from numpy import sum
import re
import operator
from sqlalchemy import *
from os import getcwd


##### BEGIN MIKE'S SEARCH CLASS #####

#import pickle
#from numpy import sum
#import re
#import operator
#from sqlalchemy import *
#from os import getcwd

basepath = getcwd()
path = "sqlite:////" + basepath + "/ratings.db"
db = create_engine(path)
conn = db.connect()
metadata = MetaData(db)
ratings = Table('ratings', metadata, autoload=True)

class N_gram_index(object):
    def __init__(self):
        with open('onegrams.pickle', 'rb') as handle:
            self.onegrams = pickle.load(handle)
        with open('twograms.pickle', 'rb') as handle:
            self.twograms = pickle.load(handle)
        with open('trigrams.pickle', 'rb') as handle:
            self.trigrams = pickle.load(handle)             
        
    def tokenize(self, text):
        text = text.lower()
        text = re.split('[^a-zA-Z0-9]', text)
        for each in text:
            if each == "":
                text.remove(each)
        return text
             
    def search(self, text):
        text = self.tokenize(text)            
        results = {}
        
        if len(text) >= 3:
            for each in self.trigrams:
                results[each] = 0
                if(text[0], text[1], text[2]) in self.trigrams[each]:
                    results[each] += 2
            for each in self.twograms:
                if (text[0], text[1]) in self.twograms[each]:
                    results[each] += 1.5
            for each in self.onegrams:
                if text[0] in self.onegrams[each]:
                    results[each] += 1
            
        elif len(text) >= 2:
            for each in self.twograms:
                results[each] = 0
                if (text[0], text[1]) in self.twograms[each]:
                    results[each] += 1.5
            for each in self.onegrams:
                if text[0] in self.onegrams[each]:
                    results[each] += 1
                    
        else:
            for each in self.onegrams:
                results[each] = 0
                if text[0] in self.onegrams[each]:
                    results[each] += 1        
        
        for each in results:
            results[each] = sum(results[each])
        topresults = {}
        for i in range(0, 5):
            maxkey = max(results.iteritems(), key=operator.itemgetter(1))[0]
            topresults[maxkey] = results[maxkey]
            del results[maxkey]
        self.printresults(topresults)
        return self.printresults(topresults)
    
    def printresults(self, topresults):
        lister = []
        for each in topresults:
            if topresults[each] > 0:
                query = ratings.select()
                query = query.where(ratings.c.prof_id == each)
                results = query.execute().first()
                print results['name'] 	#This is where it prints out the name of the teachers. 
		lister.append(results['name']) #I believe it is the only part that should need to be public.
        return lister

##### END MIKE'S SEARCH CLASS #####
