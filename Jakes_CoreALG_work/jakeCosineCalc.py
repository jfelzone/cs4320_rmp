import math

#this function calculates the similarity between two lists of tuples
#replicates a list of professors and their ratings given by the specific user
def cosineCalc(user1, user2):
    print len(user1)
    print len(user2)
    while len(user1) < len(user2):
        user1.append( ("zzz", 0.0))
    while len(user2) < len(user1):
        user2.append( ("zzz", 0.0))
    print user1
    print user2
    
    numerator = 0.0

    for index, value in enumerate(user1):
        for index2, value2 in enumerate(user2):
            if user2[index2][0] == user1[index][0]:
                numerator+= (user1[index][1]*user2[index2][1])
                print numerator
                print user1[index][1] , user2[index2][1]
    
    denominator = 0.0
    denominatorA = 0.0
    denominatorB = 0.0

    for index, value in enumerate(user1):
        denominatorA += (user1[index][1]**2)

    print "denominatorA: " , denominatorA

    for index, value in enumerate(user2):
        denominatorB += (user2[index][1]**2)

    print "denominatorB: " , denominatorB

    denominator = (math.sqrt(denominatorA) * math.sqrt(denominatorB))

    print "numerator: " , numerator
    print "denominator: " ,denominator

    print "cosine similarity:" , float(numerator/denominator)
    return float(numerator/denominator)

#this function takes a specific user and its most matching user and finds
    #recommended professors based on what the other user has taken

def collabRec(arr1, arr2):
    nullValCount=0
    for i, val in enumerate(arr2):
        nullValCount = 0
        for i2, val2 in enumerate(arr1):
             if arr1[i][0] not in arr2[i2][0]:
                 nullValCount+=1
                 if nullValCount == len(arr1):
                     print arr2[i][0]
                     
#this function takes in a combined array of all users' lists and then
            #compares it to one user and returns the most similar list
            #all based on cosine similarity
def bestUser(arr1, arr2):
    maxRel = 0.0
    indexRel=0
    for index, value in enumerate(arr2):
        if cosineCalc(arr1, value) > maxRel:
            maxRel = cosineCalc(arr1, value)
            indexRel = index
        print cosineCalc(arr1, value)
        print maxRel
        print indexRel
    return arr2[indexRel]
        
lister1 = [5.3, 0,  0,  4,  3.5, 2.3 , 5.0, 4.3, 0, 0, 0]
lister2 = [5.1, 0,  0,  4.1, 3.4, 2.3, 5.0, 4.3, 0, 0, 0]

print "Running this with two lists of just numbers first"
#this is commented out, because we are shifting to the tuple format
#print cosineCalc(lister1, lister2)

lister1 = [5.3, 0,  0,  4,  3.5, 2.3 , 5.0, 4.3]
lister2 = [5.1, 0,  0,  4.1, 3.4, 2.3, 5.0, 4.3, 0, 0, 0]

print "Testing with a smaller first list to append"
#print cosineCalc(lister1, lister2)


print "Now working on test cases with actual users"
userJake = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3)]
userJackson = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3)] 
userMike = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3), ("John Wall", 4.5)]
userJohn = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3), ("John Wall", 4.5), ("Jeremy Jones", 3.3)]
print userJake[0][0]
if userJake[0][0] == userMike [0][0]:
    print "These elements are the same"

print
print
print "Our first test"
print cosineCalc (userJake, userMike)
print
print
userJake = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3)]
userJackson = [ ("John Smith", 3.3), ("Jackson Risk", 4.5), ("Kelly Joe", 3.3)] 

print "Our second test, the data is the same"
print cosineCalc(userJake, userJackson)

print
print
userSarah = [("John Smith", 1.2), ("Tommy Tutone", 1.3), ("Tom Scott", 4.5)] 
userScott = [("John Smith", 3.2), ("Tom Scott", 4.9), ("Tommy Tutone", 4.7)] 
userTom = [("Tommy Tutone", 1.3), ("Tom Scott", 4.5), ("John Smith", 1.2) ]

userJerry = [("Tom Cruise", 2.3)]
print "Our third test, out of order data users but the same score"
print cosineCalc(userSarah, userTom)

print "Our fourth test, with different numbers but the same professors"
print cosineCalc(userSarah, userScott)

print "Our fifth test, with a user that has no relation to another"
print cosineCalc(userSarah, userJerry)

##so now one would think we could rotate and have a list of lists, and calculate
    #the cosine for each and determine which is most related

##lets make a list of lists and compare the users with a for loop using the cosine function

userOne = [("Jordan Nell", 1.4), ("John Cena", 5.0), ("Mark Wall", 4.5)]
userTwo = [("John Stamos", 2.3) , ("John Cena", 4.5), ("Mark Wall", 1.2)]
userThree = [ ("John Stamos", 2.3)]
userFour = [ ("Jordan Nell", 4.5), ("Will Farrel", 3.4)]

lister = [userTwo, userThree, userFour]
print
print lister
print
print
print "We will use the above joined list of users to determine who is most related to userOne: "


bestUser(userOne, lister)


recommendList = bestUser(userOne, lister)

print "Now here was the original list: "
print userOne
print

print "Now I would like to make a recommendation userOne: "


collabRec(userOne, recommendList)

##note to self 7:38 tuesday
        #everything is looking good. could the recommend for list be a function


##note to self 3:25 tuesday
    #the below code was all test code to arrive at the above test code... seems to work solidly
   
##so the note below is pertaining to this...
##sorted appears to be a bad way to fix this problem...
    #although maybe i should sort and then check...
#print sorted(userJake)
#print sorted(userMike)
#num = 0.0

#for index, value in enumerate(userJake):
 #   for index2, value2, in enumerate(userMike):
  #      if userMike[index2][0] == userJake[index][0]:
   #         print "we have the same here"
    #        num += userJake[index][1]*userMike[index2][1]
     #       print num

#print cosineCalc (userJake, userJohn)

##now up to this point, everything is solid, but what if we had a list that was out of order and not sorted
    #maybe i sort the lists alphabetically... hmmm.. thats not a bad idea actually
#let me google that


    


