#the following file is some simple test cases that deal with jaccard similarity and cosine similarity
    #these ideas will be used in our core algorithm for comparing the ratings of users
        #and the professors that they have had

lister1 = [1, 0, 0, 1,1,1,1,1,0,0,0]
lister2 = [1, 0, 0, 0,0,0,0,0,1,1,1]
lister3 = [0, 1]

intersection = 0.0
union = 0.0
#here i am playing around with the ideas of filling arrays when elements don't match
# i would anticipate that through this we could fill empty slots with professors that don't match
#so i need to simulate a situation where we are looking for similar professor names
    #other file may need to be alterred to fit this parameter

#so can i apply this basic super easy logic to something in our other file that is actually dealing with
    #real professors... should be fairly straight forward
    #might even generate some quick and easy sample code on this file first... might not need to though
print len(lister1)
print len(lister2)
while len(lister3) <= len(lister2):
    lister3.append(0)

print lister3
print len(lister3) , len(lister2)

for index, i in enumerate(lister1):
    if lister1[index] == 1 and lister2[index]==1:
        intersection+=1
        print "we have the same thing here baby"
    if lister1[index]==1 or lister2[index] ==1:
        union +=1


print intersection
print union

print "I am now calculating the jaccard number: "
print float(intersection/union)

print "************BREAK***********"

#also the question here is can we replicate this with a cosine similarity calculation
    #or even a pearson correlation calculation.
    #we are going to need to account for ratings to get the best results for our project
    #we recognize that it is important how people rate their professors and if similar people do the same
        #thing

lister1 = [5.3, 0, 0, 4, 3.5, 2.3 , 5.0, 4.3, 0, 0, 0]
lister2 = [3 ,  0 , 0, 0  ,0 ,0, 3.4 ,0, 3, 5 ,3]

intersection = 0.0
union = 0.0
for index, i in enumerate(lister1):
    if lister1[index] > 0 and lister2[index]>01:
        intersection+=1
        print "we have the same thing here baby"
    if lister1[index]>1 or lister2[index] >1:
        union +=1


print intersection
print union

print "I am now calculating the jaccard number: "
print float(intersection/union)



print
print
print"*******************Break*******************"
print "From here i am trying to establish and create a basic cosine similarity algorithm"
print "should result in a much more accurate value than the jaccard coefficient"


#the below code seems to correctly determine the cosine similarity for an actual rating based comparison
    #now this does not take into account zeroes....
    #therefore may not be a great way of determining our relations
    #however that being said, this is a start to compare people who are rated differently


import math

lister1 = [5.3, 0,  0,  4,  3.5, 2.3 , 5.0, 4.3, 0, 0, 0]
lister2 = [3 ,  0 , 0,  0  ,0 ,  0,    3.4 ,0,   3, 5 ,3]

#notice how when these lines are uncommented we can see just how accurate this similarity comparison is
    #obviously we are accounting for zeroes still becuase we get almost a 0.99 similarity which is insanely close
    #therefore we know cosine is a good option.
    #and in fact, the more i think about it the more i am liking this idea
    # if we are using cosine similarity we will compare two zeroes with equal weight...
        #i almost wonder if we should account for this completely and only review professors that both users have
    #rather than comparing all of them with a cosine algorithm it could be better to compare just similar ones...
    #this could be solid....

    #but then how do we recommend based on similar users.... hmmm we need zeros... i think...
    # i guess the idea is to just simply calculate this within all users and comapre and find the best users...
        #if a user is more similar we would then recommend based on professors that they do not have in common.
    #this could be viable....


    #after looking at his slides... this is exactly what we will be doing...
    #we will need to use this cosine similarity to find similar users...
    #the following file after this will implement this algorithm and then recommend based on that
    
    
lister1 = [5.3, 0,  0,  4,  3.5, 2.3 , 5.0, 4.3, 0, 0, 0]
lister2 = [5.1, 0,  0,  4.1, 3.4, 2.3, 5.0, 4.3, 0, 0, 0]

print len(lister1)
print len(lister2)

numerator = 0.0

for index, value in enumerate(lister1):
    numerator+= (lister1[index]*lister2[index])
    
denominator = 0.0
denominatorA = 0.0
denominatorB = 0.0

for index, value in enumerate(lister1):
    denominatorA += (lister1[index]**2)

print "denominatorA: " , denominatorA

for index, value in enumerate(lister2):
    denominatorB += (lister2[index]**2)

print "denominatorB: " , denominatorB

denominator = (math.sqrt(denominatorA) * math.sqrt(denominatorB))

print "numerator: " , numerator
print "denominator: " ,denominator

print "cosine similarity:" , float(numerator/denominator)
    


print "I am done" 
