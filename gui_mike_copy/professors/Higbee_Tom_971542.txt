('971542', '25873460', 'SPED5010', '5', '3', '4', 'Great class, engaging lecturer. Very helpful if you have questions about the material or homework. He LOVES his field, so the class is enjoyable.')
('971542', '22784505', 'SPED5010', '5', '4', '3', "I thought he was a great professor, his expectations were always clear, and really I would have aced the class if I had put real effort in.  Before I didn't like the subject either and now I love it! Great professor!")
('971542', '22201901', 'SPED5010', '5', '3', '5', 'Dr. Higbee is the bomb. He is hilarious, he cares about his students, and he realy loves his subject. Rarely have I had a professor that will put material on hold to answer students questions. Expectations are clear, assignments are consistent, and tests make sense. He lets you know what he expects and how he works, you just have to put in the time')
('971542', '20483559', 'SPED5310', '5', '3', '5', 'One of my favorite proffessors of all time. He is super passionate about what he is teaching and it radiates to the class, making you interested. Lectures are a MUST if you want the class to be easier. I got away with rarely reading the book but taking detailed notes in his lectures. I laughed a lot with  his little reading jokes and his excitement')
('971542', '17589158', 'SPED7720', '2', '3', '1', 'No comments')
('971542', '17028202', 'SPED5010', '5', '3', '5', "He's awesome!")
('971542', '14638618', 'SPED5530', '5', '5', '3', 'MOST ORGANIZED PROFESSOR EVER!!! I WISH ALL PROFESSORS WERE THIS ORGANIZED.')
('971542', '14538248', 'SPED7720', '2', '1', '2', 'The readings were excellent.  The professor was not.')
('971542', '13474165', 'SPED6720', '3', '1', '1', 'Seemed to understand the material well, but ability to convey it and treat students with respect clearly lacking.')
('971542', '13357869', 'SPED5530', '1', '3', '1', 'low quality teaching and rude to students.')
('971542', '13337249', 'SPEDSECT', '1', '1', '1', 'one of the worst and most condecending teachers ever.  Avoid by all means possible.')
('971542', '13331021', 'SPED7700', '1', '1', '1', "Just DON'T do it!")
('971542', '12959272', 'SPED7700', '1', '1', '1', "This professor's grading is questionable.  To get better grades one must massage his ego.  Will actually work a problem on the board and stand in front of it so that students cannot see how he gets the answer, then will act like students are &quot;stupid&quot; if they ask about it.  Avoid.")
